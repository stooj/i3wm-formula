{% from "i3wm/map.jinja" import i3wm with context %}

{% for pkg in i3wm.pkgs %}
test_{{pkg}}_is_installed:
  testinfra.package:
    - name: {{ pkg }}
    - is_installed: True
{% endfor %}
