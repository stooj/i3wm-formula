{% from "i3wm/map.jinja" import i3wm with context %}

{% for key, package in i3wm.pkgs.items() %}
i3wm_package_{{ key }}_installed:
  pkg.installed:
    - name: {{ package }}
{% endfor %}
