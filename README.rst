====
i3wm
====

Formula to install i3wm with rofi and dmenu

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.


Available states
================

.. contents::
    :local:

``i3wm``
--------

TODO - add description of this state

``i3wm.install``
-------------

Handles installation of i3wm

``i3wm.config``
-------------

Handles configuration of i3wm


Template
========

This formula was created from a cookiecutter template.

See https://github.com/mitodl/saltstack-formula-cookiecutter.
